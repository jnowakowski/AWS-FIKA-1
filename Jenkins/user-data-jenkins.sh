#!/bin/bash -xe
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins.io/redhat/jenkins.repo
rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key
yum update -y

yum install -y jenkins git java-1.8.0 java-1.8.0-devel git
yum remove -y java-1.7.0-openjdk

service jenkins start
chkconfig jenkins on

for i in {1..60}; do
  echo -n .; sleep 1;
  if (( $i % 5 == 0 )); then
      echo -n $i
  fi
done

chsh -s /bin/bash jenkins
echo "http://$(curl -s http://169.254.169.254/latest/meta-data/public-ipv4):8080/" > /tmp/jenkins-url.txt
cat /var/lib/jenkins/secrets/initialAdminPassword > /tmp/jenkins-password.txt
