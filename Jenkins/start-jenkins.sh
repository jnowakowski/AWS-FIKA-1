#!/bin/bash
set -e

INSTANCE_ID=$(aws ec2 run-instances \
	--image-id ami-70edb016 \
	--count 1 \
    --instance-type t2.micro \
    --key-name awslabs \
    --associate-public-ip-address \
    --security-group-ids sg-fd2e109b \
    --subnet-id subnet-25373b63\
    --user-data file://user-data-jenkins.sh \
    --iam-instance-profile Name=JenkinsAccess | jq -r ".Instances[].InstanceId")

aws ec2 create-tags --region eu-west-1 \
	--resources ${INSTANCE_ID} \
    --tags  Key=Name,Value=JenkinsFika

INSTANCE_PUBLIC_IP=$(aws ec2 describe-instances --instance-ids ${INSTANCE_ID} | jq -r ".Reservations[].Instances[].PublicIpAddress")
echo "############################################"
echo INSTANCE_ID=${INSTANCE_ID}
echo "http://${INSTANCE_PUBLIC_IP}:8080/"
echo "ssh -i ~/.ssh/awslabs.pem ec2-user@${INSTANCE_PUBLIC_IP}"